﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Mail;

public partial class IPN : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /* All this does is handle the Paypal Integrated Payment Notification
         * This code was all taken from:
         * https://www.paypal.com/us/cgi-bin/webscr?cmd=p/pdn/ipn-codesamples-pop-outside
         */

        //Post back to either sandbox or live
        string strSandbox = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        string strLive = "https://www.paypal.com/cgi-bin/webscr";
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strLive);

        //Set values for the request back
        req.Method = "POST";
        req.ContentType = "application/x-www-form-urlencoded";
        byte[] param = Request.BinaryRead(HttpContext.Current.Request.ContentLength);
        string strRequest = Encoding.ASCII.GetString(param);
        strRequest += "&cmd=_notify-validate";
        req.ContentLength = strRequest.Length;

        //for proxy
        //WebProxy proxy = new WebProxy(new Uri("http://url:port#"));
        //req.Proxy = proxy;

        //Send the request to PayPal and get the response
        StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII);
        streamOut.Write(strRequest);
        streamOut.Close();
        StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream());
        string strResponse = streamIn.ReadToEnd();
        streamIn.Close();

        if (strResponse == "VERIFIED")
        {
            // Get variables necessary from POST to process data (real)
            String status = HttpContext.Current.Request["payment_status"].ToString();
            String amount = HttpContext.Current.Request["mc_gross"].ToString();
            String recEmail = HttpContext.Current.Request["receiver_email"].ToString();
            String payEmail = HttpContext.Current.Request["payer_email"].ToString();
            String custom = HttpContext.Current.Request["custom"].ToString();
            
            //check the payment_status is Completed
            if (status == "Completed")
            {
                // TODO
                //check that txn_id has not been previously processed
                //check that receiver_email is your Primary PayPal email


                DataClassesDataContext db = new DataClassesDataContext();
                // Get total amount that user owes from reserved seats
                double total = (double)(from st_usr in db.seat_users
                                     join st in db.seats on st_usr.seat_id equals st.id
                                     where st_usr.user_id == Convert.ToInt32(custom)
                                     && st_usr.party_id == Convert.ToInt32(Global._eventID)
                                     && st_usr.paid == false
                                    select st.cost).Sum();

                // Get user for discount checks and emailing
                user purchaser = (from usr in db.users
                                  where usr.id == Convert.ToInt32(custom)
                                  select usr).Single();
                // Account for member discount
                if ((bool)purchaser.member)
                    total = (total - 5.0);

                if (total == Convert.ToDouble(amount)) // correct amount was paid!
                {
                    // Log the payment in the database
                    payment pmt = new payment
                    {
                        userID = Convert.ToInt32(custom),
                        paypalEmail = payEmail,
                        amount = Convert.ToDecimal(amount),
                        party_id = Convert.ToInt32(Global._eventID)
                    };
                    db.payments.InsertOnSubmit(pmt);
                    db.SubmitChanges();
                    
                    // Set all seats under the user's name to paid
                    var query = (from st_usr in db.seat_users
                                 where st_usr.user_id == Convert.ToInt32(custom)
                                 && st_usr.party_id == Convert.ToInt32(Global._eventID)
                                 && st_usr.paid == false
                                 select st_usr).ToList();
                    foreach (seat_user setMePaid in query)
                    {
                        setMePaid.paid = true;
                        setMePaid.payment_id = pmt.id;
                    }
                    db.SubmitChanges();

                    // Send email to admin
                    Global.SendEmail("catinoc@onid.orst.edu",
                        "catinoc@onid.orst.edu",
                        "Successful IPN Payment",
                        "Details" + System.Environment.NewLine
                            + "Seats purchased: " + query.Count() + System.Environment.NewLine
                            + ", User ID: " + custom + ".");

                    // Send email to user who bought the ticket(s)
                    Global.SendEmail(Convert.ToString(purchaser.email), "admin@civilwarlan4.com",
                        Convert.ToString(Global.SiteTitle + " Purchase Receipt"),
                        "Thank you for your purchase of " + query.Count() + " seats for "
                            + Convert.ToString(Global.SiteTitle) + "." + System.Environment.NewLine
                            + "You paid $" + amount + " for the seats." + System.Environment.NewLine
                            + "-Staff");
                }
                else // user outstanding payments differs from the amount paid
                {
                    // Email the admin telling him to reverse the payment.
                    Global.SendEmail("catinoc@onid.orst.edu",
                        "catinoc@onid.orst.edu",
                        "Incorrect Payment Amount",
                        "A user, " + purchaser.firstName + " " + purchaser.lastName + ", may have attempted to cheat the system.");
                    // email the user telling them it fell through.
                    Global.SendEmail(Convert.ToString(purchaser.email), "admin@civilwarlan4.com",
                        Convert.ToString(Global.SiteTitle + " - Incorrect Payment Amount"),
                        "Something went wrong and your payment amount didn't match your oustanding balance.  Please don't try to re-purchase your seats, an admin has been notified to look into this.");
                }
            }
        }
        else if (strResponse == "INVALID")
        {
            Global.SendEmail("catinoc@onid.orst.edu", "admin@civilwarlan4.com", "Invalid IPN attempt", "Someone tried, didn't work");
            //log for manual investigation
        }
        else
        {
            Global.SendEmail("catinoc@onid.orst.edu", "admin@civilwarlan4.com", "Paypal IPN didn't respond", "Read the subject");
            //log response/ipn data for manual investigation
        }
    }
}