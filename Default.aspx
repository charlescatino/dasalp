﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        #TextArea1
        {
            height: 129px;
            width: 593px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <script language="javascript" type="text/javascript" src="../Scripts/jquery-1.js"></script>
    <script language="javascript" type="text/javascript" src="../Scripts/jquery-ui.js"></script>

    <form id="form1" runat="server">
    
    <h2>LAN Announcements</h2>

    <asp:LinqDataSource ID="AnnouncementDS" runat="server" 
        ContextTypeName="DataClassesDataContext" EntityTypeName="" 
        TableName="announcements" EnableDelete="True" EnableUpdate="True" 
        OrderBy="id desc" Where="party_id == @party_id">
        <WhereParameters>
            <asp:SessionParameter Name="party_id" SessionField="eventID" Type="Int32" />
        </WhereParameters>
    </asp:LinqDataSource>

    <asp:ListView ID="AnnouncementsListView" runat="server" DataKeyNames="id" 
        DataSourceID="AnnouncementDS">
        <EditItemTemplate>
            Title: <asp:TextBox ID="titleTextBox" runat="server" Text='<%# Bind("title") %>' /><br />
            Body:
            <CKEditor:CKEditorControl ID="AnnouncementBody" runat="server" Text='<%# Bind("body") %>' />
            <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" /><br />
        </EditItemTemplate>
        <EmptyDataTemplate>
            <span>There are no announcements yet for this event.</span>
        </EmptyDataTemplate>
        <ItemTemplate>
            <h1><asp:Label ID="titleLabel" runat="server" Text='<%# Eval("title") %>' /></h1>
            <asp:Label ID="bodyLabel" runat="server" Text='<%# Eval("body") %>' />
            <% 
            if (admin) {
             %>
                <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
            <% } %>
            <br />
            <br />
        </ItemTemplate>
    </asp:ListView>

    
    <asp:PlaceHolder ID="AddAnnouncementPlaceHolder" Visible="false" runat="server">
        <br /><br /><br />
        <h2>Add a new Announcement</h2>
            <CKEditor:CKEditorControl ID="AnnouncementBody" runat="server"></CKEditor:CKEditorControl>
            <asp:Label ID="AnnouncementTitleLabel" runat="server" Text="Title:  "></asp:Label>
            <asp:TextBox ID="AnnouncementTitle" runat="server"></asp:TextBox>
            <asp:Button ID="AnnouncementAdd" runat="server" Text="Add Announcement" OnClick="addAnnouncement" ValidationGroup="newAnnouncement" /><br />
            <asp:RequiredFieldValidator
                ID="AnnouncementBodyValidator"
                runat="server"
                ErrorMessage="The Announcement Body cannot be blank"
                ControlToValidate="AnnouncementBody"
                ValidationGroup="newAnnouncement">
            </asp:RequiredFieldValidator><br />
            <asp:RequiredFieldValidator
                ID="AnnouncementTitleValidator"
                runat="server"
                ErrorMessage="You must include an Announcement Title"
                ControlToValidate="AnnouncementTitle"
                ValidationGroup="newAnnouncement">
            </asp:RequiredFieldValidator>
    </asp:PlaceHolder>
    
    </form>
    
</asp:Content>