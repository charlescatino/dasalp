﻿<%@ Page Title="Sponsors" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Sponsors.aspx.cs" Inherits="Sponsors" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" Runat="Server">
	<form id="form1" runat="server">
	<h2>Confirmed Sponsors</h2>    
    
    <asp:LinqDataSource ID="SponsorDS" runat="server" 
        ContextTypeName="DataClassesDataContext" EnableDelete="True" 
        EnableInsert="True" EnableUpdate="True" EntityTypeName="" 
        TableName="sponsors" OrderBy="priorityRanking" Where="party_id == @party_id">
        <WhereParameters>
            <asp:SessionParameter Name="party_id" SessionField="eventID" Type="Int32" />
        </WhereParameters>
    </asp:LinqDataSource>

    <asp:ListView ID="SponsorLV" runat="server" DataKeyNames="id" 
        DataSourceID="SponsorDS" InsertItemPosition="LastItem">
        <ItemTemplate>
            <div align="center" class="sponsor" id="sponsor<%# Eval("id") %>">
                <a href="<%# Eval("website") %>"><img class="sponsorIMG" src="<%# Eval("bannerImg") %>" alt="<%# Eval("imgAlt") %>" /></a>
                <br />
                <asp:Label ID="captionLabel" runat="server" Text='<%# Eval("caption") %>' />
                <br />
             <% if (admin)
                { %>
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
             <% } %>
            </div>
        </ItemTemplate>
        <EmptyDataTemplate>
            <span>No Sponsors have been added for this event yet.</span>
        </EmptyDataTemplate>
        <EditItemTemplate>
            Sponsor Name: <asp:TextBox ID="nameTextBox" runat="server" Text='<%# Bind("name") %>' />
            <br />
            Sponsor Website: <asp:TextBox ID="websiteTextBox" runat="server" Text='<%# Bind("website") %>' />
            <br />
            Side Image (URL): <asp:TextBox ID="sideImgTextBox" runat="server" Text='<%# Bind("sideImg") %>' />
            <br />
            Banner Image (URL): <asp:TextBox ID="bannerImgTextBox" runat="server" Text='<%# Bind("bannerImg") %>' />
            <br />
            Image Alternate Text: <asp:TextBox ID="imgAltTextBox" runat="server" Text='<%# Bind("imgAlt") %>' />
            <br />
            Sponsor Caption: <asp:TextBox ID="captionTextBox" runat="server" Text='<%# Bind("caption") %>' />
            <br />
            Sponsor Description: <asp:TextBox ID="descriptionTextBox" runat="server" TextMode="MultiLine"
                Text='<%# Bind("description") %>' />
            <br />
            Ranking: <asp:TextBox ID="priorityRankingTextBox" runat="server" 
                Text='<%# Bind("priorityRanking") %>' />
            <br />
            <asp:CheckBox ID="activeCheckBox" runat="server" 
                Checked='<%# Bind("active") %>' Text="Visible?" />
            <br />
            <asp:Button ID="UpdateButton" runat="server" CommandName="Update" 
                Text="Update" />
            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" 
                Text="Cancel" />
            <br /><br /></span>
        </EditItemTemplate>
        <InsertItemTemplate>
         <% if (admin)
            { %>
                <fieldset class="addSponsor">
                    <legend>Add Sponsor</legend>
                    <p>
                        Party ID: <br />
                        <asp:TextBox ID="partyIDLabel" runat="server" Value='<%# Global._eventID %>' Text='<%# Bind("party_id") %>' />
                    </p>
                    <p>
                        Sponsor Name:<br />
                        <asp:TextBox ID="nameTextBox" runat="server" CssClass="textEntry" Text='<%# Bind("name") %>' />
                    </p>
                    <p>
                        Sponsor Website:<br />
                        <asp:TextBox ID="websiteTextBox" runat="server" CssClass="textEntry" Text='<%# Bind("website") %>' />
                    </p>
                    <p>
                        Side Image:<br />
                        <asp:TextBox ID="sideImgTextBox" runat="server" CssClass="textEntry" Text='<%# Bind("sideImg") %>' />
                    </p>
                    <p>
                        Banner Image:<br />
                        <asp:TextBox ID="bannerImgTextBox" runat="server" CssClass="textEntry" Text='<%# Bind("bannerImg") %>' />
                    </p>
                    <p>
                        Image Alternate Text:<br />
                        <asp:TextBox ID="imgAltTextBox" runat="server" CssClass="textEntry" Text='<%# Bind("imgAlt") %>' />
                    </p>
                    <p>
                        Image Caption:<br />
                        <asp:TextBox ID="captionTextBox" runat="server" CssClass="textEntry" Text='<%# Bind("caption") %>' />
                    </p>
                    <p>
                        Sponsor Description:<br />
                        <asp:TextBox ID="descriptionTextBox" runat="server" CssClass="textEntry" Text='<%# Bind("description") %>' />
                    </p>
                    <p>
                        Priority Ranking:<br />
                        <asp:TextBox ID="priorityRankingTextBox" runat="server" CssClass="textEntry" Text='<%# Bind("priorityRanking") %>' />
                    </p>
                    <p>
                        <asp:CheckBox ID="activeCheckBox" runat="server" Checked='<%# Bind("active") %>' Text="Visible" />
                    </p>
                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                </fieldset>
         <% } %>
        </InsertItemTemplate>
    </asp:ListView>
    </form>


</asp:Content>