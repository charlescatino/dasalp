﻿<%@ Page Title="Information" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Information.aspx.cs" Inherits="Information" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <!--
    <h2>ABOUT</h2>
    <p>The Gaming Club is hosting its annual "Let There Be LAN" event.  We do this event each year, and it only gets better every time!</p>
    
    <h2>LOCATION</h2>
    <p>This LAN will be held in West Dining Center on campus at Oregon State University.</p>
    <a href="http://maps.google.com/maps?q=west+dining+center,+corvallis,+OR&hl=en&sll=37.0625,-95.677068&sspn=61.19447,129.814453&hnear=Marketplace+West+Dining+Center,+Corvallis,+Oregon+97331&t=m&z=17">Here is a Map</a>
	
	<h2>DATE</h2>
	<p>This LAN will run from Saturday, March 3rd to Sunday, March 4th.  Doors will open at 10am on the 3rd and close at 2am on the 4th, then reopen at 9am on the 4th for about 5 hours and then a raffle.  Exact tournament times are listed <a href="Schedule.aspx">here</a>.</p>

    <h2>SCHEDULE</h2>
    <p>See the schedule <a href="Schedule.aspx">here</a>.</p>

    <h2>TOURNAMENTS</h2>
    <p>See the tournaments <a href="Tournaments.aspx">here</a>.</p> -->

    <form id="form1" runat="server">

    <asp:LinqDataSource ID="InformationDS" runat="server" 
        ContextTypeName="DataClassesDataContext" EnableDelete="True" 
        EnableInsert="True" EnableUpdate="True" EntityTypeName="" OrderBy="weight" 
        TableName="informations" Where="party_id == @party_id">
        <WhereParameters>
            <asp:SessionParameter Name="party_id" SessionField="eventID" Type="Int32" />
        </WhereParameters>
    </asp:LinqDataSource>


    <asp:ListView ID="InformationListView" runat="server" DataKeyNames="id" DataSourceID="InformationDS">
        <EditItemTemplate>
            Title: <asp:TextBox ID="titleTextBox" runat="server" Text='<%# Bind("title") %>' /><br />
            Weight: <asp:TextBox ID="weightTextBox" runat="server" Text='<%# Bind("weight") %>' /><br />
            Body: <CKEditor:CKEditorControl ID="InformationBody" runat="server" Text='<%# Bind("body") %>' /><br />
            <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" /><br />
        </EditItemTemplate>
        <EmptyDataTemplate>
            <span>There is no information yet for this event.</span>
        </EmptyDataTemplate>
        <ItemTemplate>
            <h1><b><asp:Label ID="titleLabel" runat="server" Text='<%# Eval("title") %>' /></b></h1>
            <asp:Label ID="bodyLabel" runat="server" Text='<%# Eval("body") %>' />
         <% if (admin)
            { %>
                <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
         <% } %>
        </ItemTemplate>
    </asp:ListView>


    <asp:PlaceHolder ID="AddInformationPlaceHolder" Visible="false" runat="server">
        <br /><br /><br />
        <h2>Add a new Information section</h2>
            <CKEditor:CKEditorControl ID="InformationBody" runat="server" />
            <asp:Label ID="InformationTitleLabel" runat="server" Text="Title:  "></asp:Label>
            <asp:TextBox ID="InformationTitle" runat="server"></asp:TextBox>
            <asp:Label ID="InformationWeightLabel" runat="server" Text="Weight:  "></asp:Label>
            <asp:TextBox ID="InformationWeight" runat="server"></asp:TextBox>
            <asp:Button ID="InformationAdd" runat="server" Text="Add Information" OnClick="addInformation" ValidationGroup="newInformation" /><br />
            <asp:RequiredFieldValidator
                ID="InformationBodyValidator"
                runat="server"
                ErrorMessage="The Information Body cannot be blank"
                ControlToValidate="InformationBody"
                ValidationGroup="newInformation">
            </asp:RequiredFieldValidator><br />
            <asp:RequiredFieldValidator
                ID="InformationTitleValidator"
                runat="server"
                ErrorMessage="You must include an Information Title"
                ControlToValidate="InformationTitle"
                ValidationGroup="newInformation">
            </asp:RequiredFieldValidator><br />
            <asp:RequiredFieldValidator
                ID="InformationWeightValidator"
                runat="server"
                ErrorMessage="You must include an Information weight value"
                ControlToValidate="InformationWeight"
                ValidationGroup="newInformation">
            </asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator
            ID="InformationWeightNumberValidator"
            runat="server"
            ErrorMessage="Information weight value must be a number"
            ControlToValidate="InformationWeight"
            ValidationExpression="^\d+$">
        </asp:RegularExpressionValidator>
    </asp:PlaceHolder>
    
    </form>
</asp:Content>