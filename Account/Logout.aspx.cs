﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logout : System.Web.UI.Page
{
    public user currUser = new user();

    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Clear();
    }
}