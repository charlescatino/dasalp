﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Settings.aspx.cs" Inherits="Account_Settings" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" Runat="Server">
    Add the following stuff: Change password, change email, change username, change name?
    <form id="userAccountForm" runat="server">
        <div class="userAccount">
            <fieldset class="Username">
                <legend>Change Username</legend>
                <p>
                    <asp:Label ID="UsernameLabel" runat="server" AssociatedControlID="Username">Username:</asp:Label><br />
                    <asp:TextBox ID="Username" runat="server" CssClass="textEntry"></asp:TextBox>
                </p>
            </fieldset>
            <fieldset class="passChange">
                <legend>Change Password</legend>
                <p>
                    <asp:Label ID="PasswordOldLabel" runat="server" AssociatedControlID="PasswordOld">Previous Password:</asp:Label><br />
                    <asp:TextBox ID="PasswordOld" TextMode="Password" runat="server" CssClass="textEntry"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="PasswordNewLabel" runat="server" AssociatedControlID="PasswordNew">New Password:</asp:Label><br />
                    <asp:TextBox ID="PasswordNew" TextMode="Password" runat="server" CssClass="textEntry"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="PasswordNewRepeatLabel" runat="server" AssociatedControlID="PasswordNewRepeat">Repeat New Password:</asp:Label><br />
                    <asp:TextBox ID="PasswordNewRepeat" TextMode="Password" runat="server" CssClass="textEntry"></asp:TextBox>
                    <asp:CompareValidator ID="PasswordNewRepeatCompare"
                            ControlToValidate="PasswordNewRepeat"
                            runat="server"
                            ErrorMessage="* Passwords must match"
                            ControlToCompare="PasswordNew"
                            Operator="Equal"
                            ForeColor = "Red" />
                </p>
            </fieldset>
        </div>
        <asp:PlaceHolder ID="PlaceHolderError" runat="server"></asp:PlaceHolder><br />
        <asp:Button ID="Button1" runat="server" Text="Save Changes" onclick="saveUserInfo" />
    </form>
</asp:Content>