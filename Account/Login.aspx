﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <form id="loginForm" runat="server">
        <div class="accountInfo">
            <fieldset class="login">
                <legend>Login Information</legend>
                <p>
                    <asp:Label ID="UsernameLoginLabel" runat="server" AssociatedControlID="UsernameLogin">Username:</asp:Label>
                    <asp:TextBox ID="UsernameLogin" runat="server" CssClass="textEntry"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="PasswordLoginLabel" runat="server" AssociatedControlID="PasswordLogin">Password:</asp:Label>
                    <asp:TextBox ID="PasswordLogin" TextMode="Password" runat="server" CssClass="textEntry"></asp:TextBox>
                </p>
            <asp:PlaceHolder ID="PlaceHolderError" runat="server"></asp:PlaceHolder><br />
            </fieldset>
        </div>
        <asp:Button ID="Button1" runat="server" Text="Log In" onclick="loginUser" />
    </form>
</asp:Content>