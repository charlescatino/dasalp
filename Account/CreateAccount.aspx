﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="CreateAccount.aspx.cs" Inherits="CreateAccount" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <form id="createAccountForm" runat="server">
        <div class="accountInfo">
            <fieldset class="createAccount">
                <legend>Account Creation Information</legend>
                <p>
                    <asp:Label ID="FirstNameLabel" runat="server" AssociatedControlID="FirstName">First Name:</asp:Label><br />
                    <asp:TextBox ID="FirstName" runat="server" CssClass="textEntry"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="FirstNameValid" runat="server" ErrorMessage="* Required" ControlToValidate="FirstName"></asp:RequiredFieldValidator>
                </p>
                <p>
                    <asp:Label ID="LastNameLabel" runat="server" AssociatedControlID="LastName">Last Name:</asp:Label><br />
                    <asp:TextBox ID="LastName" runat="server" CssClass="textEntry"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="LastNameValid" runat="server" ErrorMessage="* Required" ControlToValidate="LastName"></asp:RequiredFieldValidator>
                </p>
                <p>
                    <asp:Label ID="UsernameLabel" runat="server" AssociatedControlID="Username">Username:</asp:Label><br />
                    <asp:TextBox ID="Username" runat="server" CssClass="textEntry"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="UsernameValid" runat="server" ErrorMessage="* Required" ControlToValidate="Username"></asp:RequiredFieldValidator>
                </p>
                <p>
                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label><br />
                    <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="textEntry"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="PasswordValid" runat="server" ErrorMessage="* Required" ControlToValidate="Password"></asp:RequiredFieldValidator>
                </p>
                <p>
                    <asp:Label ID="PasswordRepeatLabel" runat="server" AssociatedControlID="PasswordRepeat">Repeat Password:</asp:Label><br />
                    <asp:TextBox ID="PasswordRepeat" runat="server" TextMode="Password" CssClass="textEntry"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="PasswordRepeatValid" runat="server" ErrorMessage="* Required" ControlToValidate="PasswordRepeat"></asp:RequiredFieldValidator><br />
                    <asp:CompareValidator ID="PasswordRepeatCompare"
                            ControlToValidate="PasswordRepeat"
                            runat="server"
                            ErrorMessage="* Passwords must match"
                            ControlToCompare="Password"
                            Operator="Equal"
                            ForeColor = "Red" />
                </p>
                <p>
                    <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">Email:</asp:Label><br />
                    <asp:TextBox ID="Email" runat="server" CssClass="textEntry"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="* Required" ControlToValidate="Email"></asp:RequiredFieldValidator><br />
                    <asp:CustomValidator ID="CustomValidator1"
                    ControlToValidate="Email"
                    OnServerValidate="email_Check"
                    Display="Static"
                    ErrorMessage="* Valid email address required"
                    ForeColor = "Red"
                    runat="server" />
                </p>
                <p>
                    <asp:Label ID="EmailRepeatLabel" runat="server" AssociatedControlID="EmailRepeat">Repeat Email:</asp:Label><br />
                    <asp:TextBox ID="EmailRepeat" runat="server" CssClass="textEntry"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="EmailRepeatValid" runat="server" ErrorMessage="* Required" ControlToValidate="EmailRepeat"></asp:RequiredFieldValidator><br />
                    <asp:CompareValidator ID="EmailRepeatCompare"
                            ControlToValidate="EmailRepeat"
                            runat="server"
                            ErrorMessage="* Emails must match"
                            ControlToCompare="Email"
                            Operator="Equal"
                            ForeColor = "Red" />
                </p>
                <asp:PlaceHolder ID="PlaceHolderError" runat="server"></asp:PlaceHolder>
                <asp:Button ID="Button2" runat="server" Text="Save" onclick="saveUser" />
            </fieldset>
        </div>
    </form>
</asp:Content>