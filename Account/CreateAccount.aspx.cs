﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class CreateAccount : System.Web.UI.Page
{
    public user currUser = new user();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
            Response.Redirect("Home.aspx");
    }


    public void email_Check(object sender, ServerValidateEventArgs e)
    {
        Regex emailregex = new Regex("(?<user>[^@]+)@(?<host>.+)");
        Match myMatch = emailregex.Match(Email.Text);
        if (!myMatch.Success)
            e.IsValid = false;
    }

    protected void saveUser(object sender, EventArgs e)
    {
        Label errMsg = new Label();
        DataClassesDataContext db = new DataClassesDataContext();

        // Check to see if username is in database
        var queryU = (from usr in db.users
                      where usr.username == Username.Text
                      select usr).ToList();
        if (queryU.Any())
            errMsg.Text = "That Username has been claimed by someone else.";

        // Check to see if email is in database
        var queryE = (from usr in db.users
                      where usr.email == Email.Text
                      select usr).ToList();
        if (queryE.Any())
            errMsg.Text = "\nThat Email has been claimed by someone else.";

        if (Page.IsValid && errMsg.Text == "")
        {
            user usr = new user
            {
                firstName = FirstName.Text,
                lastName = LastName.Text,
                email = Email.Text,
                username = Username.Text,
                pass = Global.hashPassword(Password.Text),
                isAdmin = false,
                member = false
            };
            db.users.InsertOnSubmit(usr);
            db.SubmitChanges();
            Session["user"] = usr;
            Response.Redirect("~/Default.aspx");
        }
        else
        {
            PlaceHolderError.Controls.Add(errMsg);
        }
    }
}