﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_Settings : System.Web.UI.Page
{
    public user currUser = new user();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] == null)
            Response.Redirect("~/Account/Login.aspx");
        else
            currUser = (user)Session["user"];
        loadUserInfo();
    }

    protected void loadUserInfo()
    {
        // stuff

    }

    public void saveUserInfo(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        if (Username.Text != "")
        {
            var query = (from usr in db.users
                         where usr.id == currUser.id
                         select usr).Single();
            query.username = Username.Text;
            db.SubmitChanges();
        }

        String blankPass = Global.hashPassword("");

        if (Global.hashPassword(PasswordOld.Text) != blankPass)
        {
            if (Global.hashPassword(PasswordOld.Text) == currUser.pass && Global.hashPassword(PasswordNew.Text) != blankPass)
            {
                var query1 = (from usr in db.users
                              where usr.id == currUser.id
                              select usr).Single();
                query1.pass = Global.hashPassword(PasswordNew.Text);
                db.SubmitChanges();
            }
            else
            {
                // no new password applies
            }
        }
        else
        {
            // invalid old password attempt
        }
    }
}