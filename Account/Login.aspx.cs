﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class Login : System.Web.UI.Page
{
    public user currUser = new user();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
            Response.Redirect("~/Default.aspx");
    }

    protected void loginUser(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var query = from usr in db.users
                    where usr.username == UsernameLogin.Text
                    && usr.pass == Global.hashPassword(PasswordLogin.Text)
                    select usr;

        if (query.Any())
        {
            Session["user"] = currUser = (user)query.Single();
            Response.Redirect("~/Default.aspx");
        }
        else
        {
            Label errMsg = new Label();
            errMsg.Text = "Invalid Username and/or Password";
            errMsg.Font.Bold = true;
            PlaceHolderError.Controls.Add(errMsg);
        }
    }
}