﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    public bool admin = false;
    public user currUser = new user();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
        {
            currUser = (user)Session["user"];
            admin = (bool)currUser.isAdmin;
        }
        if (admin)
        {
            AddAnnouncementPlaceHolder.Visible = true;
        }

        // Set session eventID to be used for the LinqDataSource connection
        Session["eventID"] = Convert.ToInt32(Global._eventID);
    }

    protected void addAnnouncement(object sender, EventArgs e)
    {
        String announcmentText = AnnouncementBody.Text;
        announcement newAnnouncment = new announcement
        {
            body = AnnouncementBody.Text,
            party_id = Convert.ToInt32(Global._eventID),
            title = AnnouncementTitle.Text
        };
        Global.DAL.db.announcements.InsertOnSubmit(newAnnouncment);
        Global.DAL.db.SubmitChanges();
    }
}