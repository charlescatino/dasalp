﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pay : System.Web.UI.Page
{
    public user currUser = new user();
    public List<seat_user> userSeats = new List<seat_user>();
    public double seatTotal;
    public bool getsDiscount = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
            currUser = (user)Session["user"];
        else
            Response.Redirect("~/Default.aspx");

        userSeatPopulate();
        checkUser();
        LivePaypal.Visible = true;
        //TestPaypal.Visible = true;
    }

    public void userSeatPopulate()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        userSeats = (from st_usr in db.seat_users
                     where st_usr.user_id == currUser.id
                     && st_usr.party_id == Convert.ToInt32(Global._eventID)
                     && st_usr.paid == false
                     select st_usr).ToList();

        if (!userSeats.Any()) // if user doesn't have any reserved seats... redirect
            Response.Redirect("~/Register.aspx");

        seatTotal = (double)(from st_usr in db.seat_users
                     join st in db.seats on st_usr.seat_id equals st.id
                     where st_usr.user_id == currUser.id
                     && st_usr.party_id == Convert.ToInt32(Global._eventID)
                     && st_usr.paid == false
                     select st.cost).Sum();
    }

    /* Check if the user is a member, and then if they have already purchased a seat
     * Assume that if they have purchased a seat, they have used their discount */
    public void checkUser()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        if ((bool)currUser.member)
        {
            bool hasSeat = (from st_usr in db.seat_users
                            where st_usr.user_id == currUser.id
                            && st_usr.party_id == Convert.ToInt32(Global._eventID)
                            && st_usr.paid == true
                            select st_usr).Any();
            if (!hasSeat)
            {
                getsDiscount = true;
                seatTotal = seatTotal - 5;
            }
        }
    }
}