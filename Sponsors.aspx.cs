﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Sponsors : System.Web.UI.Page
{
    public bool admin = false;
    public user currUser = new user();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
        {
            currUser = (user)Session["user"];
            admin = (bool)currUser.isAdmin;
        }

        // Set session eventID to be used for the LinqDataSource connection
        Session["eventID"] = Convert.ToInt32(Global._eventID);
    }

    protected void SponsorLV_ItemInserting(object sender, ListViewInsertEventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        sponsor newSponsor = new sponsor
        {
            party_id = Convert.ToInt32(Global._eventID),
            name = e.Item.FindControl("nameTextBox").ToString(),
            website = e.Item.FindControl("websiteTextBox").ToString(),
            sideImg = e.Item.FindControl("sideImgTextBox").ToString(),
            bannerImg = e.Item.FindControl("bannerImgTextBox").ToString(),
            imgAlt = e.Item.FindControl("imgAltTextBox").ToString(),
            caption = e.Item.FindControl("captionTextBox").ToString(),
            description = e.Item.FindControl("descriptionTextBox").ToString(),
            priorityRanking = Convert.ToInt32(e.Item.FindControl("priorityRankingTextBox").ToString()),
            active = true
        };
        db.sponsors.InsertOnSubmit(newSponsor);
        db.SubmitChanges();
    }
}