﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Web.Security;
using System.Configuration;

/// <summary>
/// Contains my site's global variables.
/// </summary>
public static class Global
{
    public static int _eventID = 1;
	public static DAL DAL = new DAL();

    public static string SiteTitle
    {
        get
        {
            DataClassesDataContext db = new DataClassesDataContext();
            return (from prty in db.parties
                    where prty.id == Convert.ToInt32(_eventID)
                    select prty).Single().name;
        }
        set
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var query = (from prty in db.parties
                         where prty.id == Convert.ToInt32(_eventID)
                         select prty).Single();
            query.name = value;
            db.SubmitChanges();
        }
    }

    public static string PaypalEmail
    {
        get
        {
            DataClassesDataContext db = new DataClassesDataContext();
            return (from prty in db.parties
                    where prty.id == Convert.ToInt32(_eventID)
                    select prty).Single().paypal;
        }
        set
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var query = (from prty in db.parties
                         where prty.id == Convert.ToInt32(_eventID)
                         select prty).Single();
            query.paypal = value;
            db.SubmitChanges();
        }
    }

    public static int LayoutID
    {
        get
        {
            DataClassesDataContext db = new DataClassesDataContext();
            return (int)(from prty in db.parties
                    where prty.id == Convert.ToInt32(_eventID)
                    select prty).Single().layoutID;
        }
        set
        {
            DataClassesDataContext db = new DataClassesDataContext();
            var query = (from prty in db.parties
                         where prty.id == Convert.ToInt32(_eventID)
                         select prty).Single();
            query.layoutID = Convert.ToInt32(value);
            db.SubmitChanges();
        }
    }

    public static void SendEmail(string toEmail, string fromEmail, string subject, string message)
    {
        MailAddress addr = new MailAddress(toEmail);
        MailAddress from = new MailAddress(fromEmail);
        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
        msg.Sender = from;
        msg.To.Add(addr);
        msg.From = from;
        msg.Subject = subject;
        msg.Body = message;
        msg.IsBodyHtml = true;

        SmtpClient client = new SmtpClient();
        client.Send(msg);
    }

    public static String hashPassword(string password)
    {
        return FormsAuthentication.HashPasswordForStoringInConfigFile(password,
            System.Web.Configuration.FormsAuthPasswordFormat.SHA1.ToString());
    }
}