﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DAL
/// </summary>
public class DAL
{
	public DataClassesDataContext db = new DataClassesDataContext();

	public DAL()
	{
		//
		// TODO: Add constructor logic here
		//
	}

	public organization getOrgByURL(string orgURL)
	{
		return (from org in db.organizations
			   where org.url_name == orgURL
			   select org).Single();
	}

	public party getPartyByURL(string partyURL)
	{
		return (from prty in db.parties
				where prty.party_url == partyURL
				select prty).Single();
	}
}