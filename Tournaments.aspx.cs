﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tournaments : System.Web.UI.Page
{
    public bool admin = false;
    public user currUser = new user();
    public List<tournament> tourneys = new List<tournament>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
        {
            currUser = (user)Session["user"];
            admin = (bool)currUser.isAdmin;
        }
        if (admin)
        {
            //AddInformationPlaceHolder.Visible = true;
        }

        // Set session eventID to be used for the LinqDataSource connection
        Session["eventID"] = Convert.ToInt32(Global._eventID);
    }
}