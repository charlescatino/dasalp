﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class SeatSelection : System.Web.UI.Page
{
    public List<table> myTables = new List<table>();
    public user currUser = new user();
    public TimeSpan reservedSeatOld = new TimeSpan(0, 60, 0);

    protected void Page_Load(object sender, EventArgs e)
    {

        prepRegister();
    }

    protected void prepRegister()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        // Populate myTables object with the tables for the layout
        myTables = (from tbl in db.tables
                    where tbl.layoutID == Global.LayoutID
                    select tbl).ToList();
    }

    protected string getClientScript()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        // Build a string
        StringBuilder sb = new StringBuilder();

        // TODO: Add table view in DB to make this query and string building
        // less object-hop necessary.

        // Get all seats associated with this party, layout, that are either:
        // A) Reserved and not past due
        // B) Paid for
        var activeSeats = (from st_usr in db.seat_users
                             where st_usr.layout_id == Global.LayoutID
                             && st_usr.party_id == Global._eventID
                             && (st_usr.date_reserved < DateTime.Now.Subtract(reservedSeatOld) || st_usr.paid == true)
                             select st_usr).ToList();
        foreach (seat_user st_usr in activeSeats)
        {
            // Build strings for Javascript
            // Goal: addSeat(seatID, seatName, seatCost, tableName, username, status);
            sb.Append("addSeat(" + st_usr.seat_id + ", "
                    + "'" + st_usr.seat.name + "', "
                    + Convert.ToString(st_usr.seat.cost) + ", "
                    + "'" + st_usr.seat.table.name + "', "
                    + "'" + st_usr.user.username + "'");
            if (st_usr.user_id != currUser.id && st_usr.paid == false) // reserved
                sb.Append(", 0);\n");
            if (st_usr.user_id == currUser.id && st_usr.paid == false) // user reserved
                sb.Append(", 1);\n");
            if (st_usr.user_id != currUser.id && st_usr.paid == true) // paid
                sb.Append(", 2);\n");
            if (st_usr.user_id == currUser.id && st_usr.paid == true) // user paid
                sb.Append(", 3);\n");
        }
        return Convert.ToString(sb);
    }
}