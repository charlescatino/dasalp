﻿<%@ Page Title="Seat Selection" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SeatSelection.aspx.cs" Inherits="SeatSelection" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" Runat="Server">
    <!-- Scripts -->
    <script language="javascript" type="text/javascript" src="./Scripts/seatSelection2.js"></script>
    <script language="javascript" type="text/javascript" src="./Scripts/Seat.js"></script>
    <script language="javascript" type="text/javascript" src="./Scripts/jquery-1.js"></script>
    <script language="javascript" type="text/javascript" src="./Scripts/jquery-ui.js"></script>
    <script language="javascript" type="text/javascript">
        window.onload = function () {
            // set reference to the div element after page is loaded
            div = document.getElementById('hoverbox');

            // populate any existing reserved seats for user
            <% Response.Write(this.getClientScript()); %>
        }
        /*function populateCustomValue(userID) {
            // Set Paypal custom hidden value to "<userID>:<selected dropdown index>"
            document.getElementById("HiddenPaypal").value = (userID + ":" + document.getElementById("payID").selectedIndex);
        } */
    </script>
    <div class="regBG">

    <asp:PlaceHolder ID="RegErrorPlaceholder" Visible="false" runat="server">
        <div id="RegErrorDiv" class="regError">
            Someone reserved one or more of the seats that you had selected.  The rest have been reserved for you.  Please review your current reservation(s) before proceeding.
        </div>
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="seating" runat="server">
        <div id="layoutArea" style="position:relative;">
        <%
            foreach (table tbl in this.myTables)
            {
                int i, j, row_num, cell_num, currSeat = 0;
                string tableStyle = "style=\"position:absolute; left:" + tbl.leftOffset + "px; top:"
                                + tbl.topOffset + "px;\"";
                if (tbl.orientation == "Horizontal")
                {
                    row_num = tbl.width;
                    cell_num = tbl.length;
                }
                else
                {
                    row_num = tbl.length;
                    cell_num = tbl.width;
                } %>
                <div class="tableContent" <%= tableStyle %>>
                <table class="seating">
                    Table <%= tbl.name %>
                    <% for (i = 0; i < row_num; i++)
                       { %>
                            <tr>
                            <% for (j = 0; j < cell_num; j++)
                               { // assume seat is empty by default %>
                                    <td class="available seat" 
                                        id="<%= tbl.seats[currSeat].id %>"
                                        onclick="seatClicked(this)"
                                        onmouseout="seatinfo_hide()"
                                        onmouseover="seatinfo_show(this, '<%= tbl.name %>', '<%= tbl.seats[currSeat].name %>', null, false)"
                                    />
                            <%      currSeat++;
                               } %>
                            </tr>
                    <% } %>
                </table>
                </div>
            <%} %>
        </div>
    </asp:PlaceHolder>
    <div id="hoverbox"></div>
    </div>
</asp:Content>
