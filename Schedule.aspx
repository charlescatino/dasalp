﻿<%@ Page Title="Schedule" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Schedule.aspx.cs" Inherits="Schedule" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Schedule - TENTATIVE</h2>
    <p>This is the tentative schedule for the LAN.</p>
    <p>Friday, May 18th</p>
    <ul>
        <li>6pm - Open</li>
        <li>8pm - CounterStrike Tournament Start</li>
    </ul>
    <p>Saturday, May 19th</p>
    <ul>
        <li>1pm - Dota 2 Tournament Start</li>
        <li>7pm - StarCraft 2 Tournament Start</li>
    </ul>
    <p>Sunday, May 20th</p>
    <ul>
        <li>3am - Close (break)</li>
        <li>9am - Reopen (from break)</li>
        <li>3pm - Raffle</li>
        <li>4:00pm - Cleanup</li>
    </ul>
</asp:Content>