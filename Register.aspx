﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" Runat="Server">
    <!-- Scripts -->
    <script language="javascript" type="text/javascript" src="./Scripts/seatSelection.js"></script>
    <script language="javascript" type="text/javascript" src="./Scripts/Seat.js"></script>
    <script language="javascript" type="text/javascript" src="./Scripts/jquery-1.js"></script>
    <script language="javascript" type="text/javascript" src="./Scripts/jquery-ui.js"></script>
    <script language="javascript" type="text/javascript">
        window.onload = function () {
            // set reference to the div element after page is loaded
            div = document.getElementById('hoverbox');

            // populate any existing reserved seats for user
            <% Response.Write(this.getClientScript()); %>
        }
        function populateCustomValue(userID) {
            // Set Paypal custom hidden value to "<userID>:<selected dropdown index>"
            document.getElementById("HiddenPaypal").value = (userID + ":" + document.getElementById("payID").selectedIndex);
        }
    </script>
    <div class="regBG">
    <asp:PlaceHolder ID="RegErrorPlaceholder" Visible="false" runat="server">
        <div id="RegErrorDiv" class="regError">
            Someone reserved one or more of the seats that you had selected.  The rest have been reserved for you.  Please review your current reservation(s) before proceeding.
        </div>
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="seating" Visible="false" runat="server">
    <div id="layoutArea" style="position:relative;">
    <%
        foreach (table tbl in this.myTables)
        {
            int i, j, row_num, cell_num, currSeat = 0;
            string style = "style=\"position:absolute; left:" + tbl.leftOffset + "px; top:"
                            + tbl.topOffset + "px;\"";
            if (tbl.orientation == "Horizontal")
            {
                row_num = tbl.width;
                cell_num = tbl.length;
            }
            else
            {
                row_num = tbl.length;
                cell_num = tbl.width;
            } %>
            <div class="tableCont" <% Response.Write(style); %>>Table <% Response.Write(tbl.name); %>
            <table class="seating">
                <% for (i = 0; i < row_num; i++)
                   { %>
                       <tr>
                       <% for (j = 0; j < cell_num; j++)
                          {
                              seat tblCurrSeat = tbl.seats[currSeat];
                              seat_user currSeatUser = null;
                              if (tblCurrSeat.seat_users.Any())
                                  currSeatUser = tblCurrSeat.seat_users[0]; %>
                              <% // GOAL: seatinfo_show(obj, tableName, seatName, username, paid) %>
                              <td onmouseover="seatinfo_show(this, <% Response.Write("\'"+tbl.name+"\'"); %>,
                              <% Response.Write("\'"+tblCurrSeat.name+"\'"); %>,
                              <% if(currSeatUser != null) { // if seat is selected by any user
                                    Response.Write("\'"+currSeatUser.user.username+"\'"); %>,
                                 <% if(currSeatUser.paid) { // if seat has been paid for
                                        Response.Write("true");
                                    } else {
                                        Response.Write("false");
                                    }
                                 } else {
                                    Response.Write("null, false");
                                 } %>)"
                              onmouseout="seatinfo_hide()"
                              <% if(currSeatUser != null) { // if someone has the seat
                                    if(currSeatUser.user_id == currUser.id) { // seat is user's seat
                                        if(!currSeatUser.paid) { // seat is reserved, not paid %>
                                            class="userReserved seat"
                                            onclick="seatClicked(<% Response.Write(tblCurrSeat.id); %>,
                                                <% Response.Write("\'"+tblCurrSeat.name+"\'"); %>,
                                                <% Response.Write(tbl.id); %>,
                                                <% Response.Write("\'"+tbl.name+"\'"); %>,
                                                <% Response.Write(tblCurrSeat.cost); %>)"
                                     <% } else { // seat is paid for by user %>
                                            class="userTaken seat"
                                     <% }
                                    } else {
                                        if(!currSeatUser.paid) { // seat is reserved, not paid %>
                                            class="reserved seat"
                                     <% } else { // seat is paid for by some user %>
                                            class="taken seat"
                                     <% }
                                    }
                                 } else { // nobody has the seat yet! 
                                    if (currUser.id > 0) { // valid user logged in %>
                                        onclick="seatClicked(<% Response.Write(tblCurrSeat.id); %>,
                                        <% Response.Write("\'"+tblCurrSeat.name+"\'"); %>,
                                        <% Response.Write(tbl.id); %>,
                                        <% Response.Write("\'"+tbl.name+"\'"); %>,
                                        <% Response.Write(tblCurrSeat.cost); %>)"
                                 <% } else { // nobody is logged in currently %>
                                        onclick="alert('You must be logged in to select seats.')"
                                 <% } %>
                                    class="available seat"
                              <% } %>
                              id="<% Response.Write(tbl.name); Response.Write(tblCurrSeat.name); %>"
                              ></td>
                       <%     currSeat++;
                          } %>
                       </tr>
                <% } %>
           </table>
           </div>
        <% } %>
    </div>

    <div id="rightLeg">
        <asp:PlaceHolder ID="ShoppingCartPlaceHolder" Visible="false" runat="server">
            <fieldset class="cart">
                <legend>Seat Options</legend>
                <form id="Form1" runat="server">
                    <div>
                        <input ID="Hidden1" runat="server" type="hidden" />
                        <asp:Label ID="chosenMessage" runat="server" Text="Label">No seats selected.</asp:Label><br /><br />
                        <asp:Button ID="confirm" runat="server" OnClientClick="if (!saveSeats()) return false;" onclick="userConfirm" Disabled="true" Text="Reserve/Purchase Seats" />
                        <asp:Button ID="ClearReserved" runat="server"
                            OnClientClick="if(!confirm('Are you sure you want remove all of your currently reserved seats?')) return false"
                            OnClick="clearReserved" Text="Clear Reserved Seats" />
                        <br />
                    </div>
                </form>
            </fieldset>
        </asp:PlaceHolder>
        <fieldset class="legend">
            <legend>Legend</legend>
            <table id="legend">
                <tr ><td colspan="3"><asp:Label ID="TotalSeats" runat="server" Text="" /></td></tr>
                <tr>
                    <td class="available seat"</td>
                    <td>Available seat(s)</td>
                    <td><asp:Label ID="AvailableSeats" runat="server" Text="" /></td>
                </tr>
                <tr>
                    <td class="reserved seat"></td>
                    <td>Reserved seat(s)*</td>
                    <td><asp:Label ID="ReservedSeats" runat="server" Text="" /></td>
                </tr>
                <% if (currUser.id > 0)
                   { %>
                <tr><td class="userReserved seat"</td><td>Your reserved seat(s)</td><td></td></tr>
                <tr><td class="userTaken seat"</td><td>Your paid seat(s)</td><td></td></tr>
                <% } %>
                <tr>
                    <td class="taken seat"</td>
                    <td>Paid seat(s)</td>
                    <td><asp:Label ID="PaidSeats" runat="server" Text="" /></td>
                </tr>
                <tr><td colspan="3">*Reserved seats are reset if not paid for within 60 minutes.</td></tr>
            </table>
        </fieldset>
    </div>
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="consoleSeating" Visible="false" runat="server">
    <p>You have registered as Console and you have paid.  Thanks!  We'll see you on the 3rd!</p>
    </asp:PlaceHolder>
    <div id="hoverbox"></div>








    <!-- OLD CONTENT DOWN HERE
    <asp:PlaceHolder ID="registrationFull" Visible="false" runat="server">
        Registration for this event is full.  Better luck next time!  Console can pay at the door, since I'm too lazy to code for that...<br />
        Cost for Console: $10.  Please bring cash and remember, this is a BYOG (Bring Your Own Gear) event.  Please bring AT LEAST a controller to play with.  Ideally bring a console and TV. :)
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="notLoggedIn" Visible="false" runat="server">
        You must be logged in to view this page. <br />
        You can <a href="~/Account/Login.aspx">Login here</a> or <a href="~/Account/CreateAccount.aspx">Create an Account</a>.
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="notPaidTesting" Visible="false" runat="server">
        You must pay before you can choose a seat. PAYPAL SANDBOX
        <form id="form2" runat="server"><asp:Button ID="Button1" runat="server" Text="I don't want to pay" OnClick="payOverride" /></form>

        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
	        <input type="hidden" name="cmd" value="_s-xclick">
	        <input type="hidden" name="hosted_button_id" value="D5VVH52CKDK32">
	        <table>
	        <tr><td><input type="hidden" name="on0" value="Choose carefully...">Choose carefully...</td></tr><tr><td><select id="payID" name="os0">
		        <option value="PC Admission">PC Admission $15.00 USD</option>
		        <option value="Console Admission">Console Admission $10.00 USD</option>
		        <option value="PC Admission - Members">PC Admission - Members $10.00 USD</option>
		        <option value="Console Admission - Members">Console Admission - Members $5.00 USD</option>
		        <option value="PC Admission + Membership Fee">PC Admission + Membership Fee $30.00 USD</option>
		        <option value="Console Admission + Membership Fee">Console Admission + Membership Fee $25.00 USD</option>
	        </select> </td></tr>
	        </table>
	        <input type="hidden" name="currency_code" value="USD">
            <input id="HiddenPaypal" name="custom" type="hidden" value="MyTestValue" />
	        <input type="image" onclick="populateCustomValue(<% Response.Write(currUser.id); %>)" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	        <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </form>
        <button onclick="populateCustomValue(<% Response.Write(currUser.id); %>)" text="button"></button>

    </asp:PlaceHolder>

    <asp:PlaceHolder ID="notPaid" Visible="false" runat="server">
        You must pay before you can choose a seat.  If you have any issues with Paypal, please don't try and pay again.  Please contact Charles Catino at <a href="mailto:catinoc@onid.orst.edu">catinoc@onid.orst.edu</a>
        <p style="color:Red; font-weight:bold; font-size:14pt;">READ CAREFULLY: There are 2 types of seats with 3 payment options each.</p>
        <ul>
            <li>PC Admission - This is for non-members who want a PC seat</li>
            <li>Console Admission - This is for non-members who want a console ticket</li>
            <li>PC Admission (Members) - This is for Gaming Club paid members who want a PC seat (if you don't know if you're a member or not, then you aren't).</li>
            <li>Console Admission (Members) - This is for Gaming Club members who want a Console ticket (if you don't know if you're a member or not, then you aren't).</li>
            <li>PC Admission (+ Membership Fee) - This is for non-members who want a PC seat and want to become a member.</li>
            <li>Console Admission (+ Membership Fee) - This is for non-members who want a Console ticket and want to become a member.</li>
        </ul>

        <h3>What does my yearly Gaming Club Membership get me?</h3>
        <ul>
            <li>Priority registration for Gaming Club LAN parties.</li>
            <li>$5 off admission for each LAN party that the Gaming Club hosts that year.</li>
            <li>Voting rights on LAN party tournaments and activities.</li>
            <li>Discount on Gaming Club t-shirts each year.</li>
            <li>Future benefits: Members only raffle prizes, extra tickets, reserved slots on servers.</li>
        </ul>

        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="YDEXP7A6C345Q">
            <table>
            <tr><td><input type="hidden" name="on0" value="Choose carefully...">Choose carefully...</td></tr><tr><td><select id="payID" name="os0">
	            <option value="PC Admission">PC Admission $15.00 USD</option>
	            <option value="Console Admission">Console Admission $10.00 USD</option>
	            <option value="PC Admission - Members">PC Admission - Members $10.00 USD</option>
	            <option value="Console Admission - Members">Console Admission - Members $5.00 USD</option>
	            <option value="PC Admission + Membership Fee">PC Admission + Membership Fee $30.00 USD</option>
	            <option value="Console Admission + Membership Fee">Console Admission + Membership Fee $25.00 USD</option>
            </select> </td></tr>
            </table>
            <input type="hidden" name="currency_code" value="USD">
            <input id="HiddenPaypal" name="custom" type="hidden" value="stuff" />
            <input type="image" onclick="populateCustomValue(<% Response.Write(currUser.id); %>)" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
         </form>

         <h3>What exactly does this ticket get me..?</h3>
         <ul>
            <li>Admission to the LAN (20ish hours of gaming goodness).</li>
            <li>Pizza on Saturday night.  Coffee in the morning.</li>
            <li>Buy one, get one free CHIPOTLE all weekend.</li>
            <li>A ticket to the raffle which will have around $1000 worth of prizing!</li>
         </ul>
    </asp:PlaceHolder> -->
    </div>
</asp:Content>