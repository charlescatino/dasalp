﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class Register : System.Web.UI.Page
{
    public user currUser = new user();
    public layout currLayout = new layout();
    public List<table> myTables = new List<table>();
    public List<merchandise> myMerch = new List<merchandise>();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
            refreshUser(); // this is necessary since this page relies on "setting paid" for user in DB
        currLayout.id = Convert.ToInt32(Global.LayoutID);
        removeOldReserved();
        //setActiveContents();
        seating.Visible = true;
        prepareRegister();
        regStatistics();
    }

    /* Remove reserved seats that are more than 10 minutes old and not paid for */
    protected void removeOldReserved()
    {
        TimeSpan timeDiff = new TimeSpan(0, 60, 0);
        DataClassesDataContext db = new DataClassesDataContext();
        var query = (from st_usr in db.seat_users
                     where st_usr.party_id == Convert.ToInt32(Global._eventID)
                     && st_usr.layout_id == Global.LayoutID
                     && st_usr.paid == false
                     select st_usr);
        foreach (seat_user currStUsr in query)
        {
            if (currStUsr.date_reserved < DateTime.Now.Subtract(timeDiff))
                db.seat_users.DeleteOnSubmit(currStUsr);
        }
        db.SubmitChanges();
    }

    protected void regStatistics()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var currentLayout = (from lyt in db.layouts
                        where lyt.id == Global.LayoutID
                        select lyt).Single();
        TotalSeats.Text = Convert.ToString("Total Seats: " + currentLayout.numSeats);

        var paidSeats = (from st_usr in db.seat_users
                         where st_usr.party_id == Convert.ToInt32(Global._eventID)
                         && st_usr.paid == true
                         && st_usr.layout_id == Global.LayoutID
                         select st_usr).ToList();
        PaidSeats.Text = Convert.ToString(paidSeats.Count());

        var reservedSeats = (from st_usr in db.seat_users
                             where st_usr.party_id == Convert.ToInt32(Global._eventID)
                             && st_usr.paid == false
                             && st_usr.layout_id == Global.LayoutID
                             select st_usr).ToList();
        ReservedSeats.Text = Convert.ToString(reservedSeats.Count());

        AvailableSeats.Text = Convert.ToString(currentLayout.numSeats - paidSeats.Count() - reservedSeats.Count());
    }

    protected void refreshUser()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var refreshUser = (from usr in db.users
                           where usr.id == ((user)Session["user"]).id
                           select usr).Single();
        currUser = (user)refreshUser;
        ShoppingCartPlaceHolder.Visible = true;
    }


    protected void prepareRegister()
    {
        // Populate myTables object with the tables for the layout
        DataClassesDataContext db = new DataClassesDataContext();
        myTables = (from tbl in db.tables
                     where tbl.layoutID == Global.LayoutID
                     select tbl).ToList();
        // Check to see if there was a registration error
        if (Session["regError"] != null && (bool)Session["regError"])
        {
            RegErrorPlaceholder.Visible = true;
            Session["regError"] = false;
        }
    }

    protected string getClientScript()
    {
        // Check if user has seat(s) already reserved (not paid) for the current layout.
        //      Populate javascript functions to update shopping cart
        DataClassesDataContext db = new DataClassesDataContext();
        var userReservedSeats = (from st_usr in db.seat_users
                                 where st_usr.layout_id == Global.LayoutID
                                 && st_usr.user_id == currUser.id
                                 && st_usr.paid == false
                                 && st_usr.party_id == Convert.ToInt32(Global._eventID)
                                 select st_usr).ToList();
        StringBuilder sb = new StringBuilder();
        foreach (seat_user st_usr in userReservedSeats)
        {
            sb.Append("addReservedSeat(" + st_usr.seat_id + ",'" + st_usr.seat.name + "',"
                            + st_usr.seat.tableID + ",'" + st_usr.seat.table.name + "'," + st_usr.seat.cost + ");\n");
        }
        sb.Append("updateCart();\n");

        return sb.ToString();
    }

    protected void debugUser()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var query = (user)(from usr in db.users
                           where usr.id == 0
                           select usr).Single();
        currUser = query;
    }

    public void userConfirm(object sender, EventArgs e)
    {
        bool userGotAllSeats = true;
        // Get IDs of all seats that the user selected from the hidden field
        // FORMAT: seatid:seatid:REPEAT
        string[] selectedSeats = Hidden1.Value.Trim(':').Split(':');

        DataClassesDataContext db = new DataClassesDataContext();

        // TODO: Check to make sure seat isn't taken...

        // Delete all previous (non-paid) user seat selections from DB
        var delSeats = (from st_usr in db.seat_users
                        where st_usr.user_id == currUser.id
                        && st_usr.layout_id == Global.LayoutID
                        && st_usr.party_id == Convert.ToInt32(Global._eventID)
                        && st_usr.paid == false
                        select st_usr).ToList();
        db.seat_users.DeleteAllOnSubmit(delSeats);
        db.SubmitChanges();

        // Add user seat selections to Database
        foreach (string seatID in selectedSeats)
        {
            var checkSeatAvailable = (from st_usr in db.seat_users
                                      where st_usr.seat_id == Convert.ToInt32(seatID)
                                      && st_usr.party_id == Convert.ToInt32(Global._eventID)
                                      && st_usr.layout_id == Global.LayoutID
                                      select st_usr).ToList();
            if (checkSeatAvailable.Any())
            {
                // someone else snagged that seat while seat selection was occurring..
                userGotAllSeats = false;
            }
            else
            {
                seat_user newStUsr = new seat_user
                {
                    seat_id = Convert.ToInt32(seatID),
                    user_id = currUser.id,
                    layout_id = Global.LayoutID,
                    party_id = Convert.ToInt32(Global._eventID),
                    date_reserved = DateTime.Now
                };
                db.seat_users.InsertOnSubmit(newStUsr);
            }
        }
        db.SubmitChanges();

        if (userGotAllSeats)  // Take user to payment page
            Response.Redirect("Pay.aspx");
        else
        {
            // refresh with error and seats selected
            Session["regError"] = true;
            Response.Redirect("Register.aspx");
        }
    }

    /**
     * Remove all reserved seats (non-paid) that are selected by the currently
     * logged in user for the active plan
     */
    public void clearReserved(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var query = (from st_usr in db.seat_users
                     where st_usr.user_id == currUser.id
                     && st_usr.layout_id == Global.LayoutID
                     && st_usr.paid == false
                     select st_usr).ToList();
        db.seat_users.DeleteAllOnSubmit(query);
        db.SubmitChanges();
    }


    /* OLD CODE BELOW THIS POINT
     * 10101010101010101010101010
     * 10101010101010101010101010
     * /
    /*protected void setActiveContents()
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var numPaid = (from usr in db.users
                       where usr.isPaid > 0
                       && (usr.isPaid == 1
                       || usr.isPaid == 3
                       || usr.isPaid == 5)
                       select usr).ToList();
        
        if (currUser.id == 0)
            notLoggedIn.Visible = true;
        if (currUser.id > 0)
        {
            if (currUser.isPaid == 0)
            {
                if (numPaid.Count > 111)
                {
                    // Registration is full
                    registrationFull.Visible = true;
                }
                else
                {
                    notPaid.Visible = true;
                    //notPaidTesting.Visible = true; // Paypal Sandbox
                }
            }
            else
            {
                if (currUser.isPaid == 1 || currUser.isPaid == 3 || currUser.isPaid == 5)
                {
                    prepareRegister();
                    regStatistics();
                    seating.Visible = true;
                }
                else
                {
                    consoleSeating.Visible = true;
                }
            }
        }
    } */

    public void payOverride(object sender, EventArgs e)
    {
        //    DataClassesDataContext db = new DataClassesDataContext();
        //    var query = (from usr in db.users
        //                    where usr.id == currUser.id
        //                    select usr).Single();
        //    query.isPaid = 1;
        //    db.SubmitChanges();
        //    Response.Redirect("Register.aspx");
    }
}