﻿/*
    Seat

    This basically mirrors the Seat object in the database.

    @id - ID of the seat.  This is the key value of the seat object in the db.
    @name - Name of the seat. String type, usually a letter or number.  Identifier in the table.
    @tableID - ID of the table that the seat belongs to
    @tableName - Name of the table that the seat belogns to.  Usually a letter or number.
    @price - Cost of the seat to the user.

    */

function Seat(seatID, seatName, seatTableName, cost) {
    this.id = seatID;
    this.name = seatName;
    //this.tableID = seatTableID;
    this.tableName = seatTableName;
    this.price = cost;
}