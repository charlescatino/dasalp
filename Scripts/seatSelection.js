﻿// define reference to the hidden div element
var div;
//var formDiv;
var lastSeat;
//var startX, startY;
seats = new Array();
var totalPrice = 0;

// show hoverbox w/ info
function seatinfo_show(obj, tableName, seatName, username, paid) {
    var oTop = 0, oLeft = 0;

    // prepare position
    do {
        oLeft += obj.offsetLeft;
        oTop += obj.offsetTop;
    } while (obj = obj.offsetParent);

    // set the position of invisible div
    div.style.top = (oTop + 20) + 'px';
    div.style.left = (oLeft + 20) + 'px';

    div.innerHTML = "Seat: " + tableName + "-" + seatName;
    if (username != null)
        div.innerHTML += "<br>" + username;
    if (paid == "false")
        div.innerHTML += "<br>RESERVED";

    // show hoverbox
    div.style.visibility = 'visible';
}

// hide hoverbox (hide div element)
function seatinfo_hide() {
    div.style.visibility = 'hidden';
}

// add seats selected to list via C# population of user reserved seats
function addReservedSeat(seatID, seatName, seatTableID, seatTableName, seatCost) {
    seats.push(new Seat(seatID, seatName, seatTableID, seatTableName, seatCost));
    totalPrice = totalPrice + seatCost;
}

// User selects a seat
function seatClicked(seatID, seatName, seatTableID, seatTableName, seatCost) {
    var remove = false;
    // update list of seats
    for (i = 0; i < seats.length; i++) {
        if (seatID == seats[i].id) {
            remove = true;
            // seat already in list, remove it
            seats.splice(i, 1);
            // Un-highlight clicked seat
            document.getElementById('' + seatTableName + seatName + '').setAttribute("class", "seat available"); // style.backgroundColor = 'green';
            totalPrice = totalPrice - seatCost;
        }
    }
    if (!remove) {
        addReservedSeat(seatID, seatName, seatTableID, seatTableName, seatCost);
        // highlight newly selected seat
        document.getElementById('' + seatTableName + seatName + '').setAttribute("class", "seat userReserved"); //.style.backgroundColor = 'Blue';
    }
    updateCart();
}

// User selects a merchandise option
function merchCheck(obj, cost) {
    if (obj.checked)
        totalPrice = totalPrice + cost;
    else
        totalPrice = totalPrice - cost;
    updateCart();
}

function updateCart() {
    // Update price total to user
    sb = "Your current total is: $" + totalPrice + "";
    document.getElementById("MainContent_chosenMessage").innerHTML = sb;

    // set purchase button available if total is greater than 0
    if (seats.length > 0)
        document.getElementById("MainContent_confirm").disabled = false;
    else
        document.getElementById("MainContent_confirm").disabled = true;
}

// User wants to save their seats!
function saveSeats() {
    // Ensure that at least one seat has been selected
    if (seats.length == 0) {
        alert("You haven't selected any seats!");
        return false;
    }

    // Build string to pass to C# via hidden field
    // FORMAT: seatid:seatid:REPEAT
    var sb = "";
    for (var s in seats) {
        sb = sb + seats[s].id + ":";
    }

    // Put that string into the hidden field value
    document.getElementById("MainContent_Hidden1").value = sb;

    return true;
}

/*
function selectSeat(tableID, tableName, seatID, seatNum) {
    var sb = "";

    // Enable form confirmation button
    document.getElementById("MainContent_confirm").disabled = false;

    // Clear last selected seat
    if (lastSeat != null)
        document.getElementById(lastSeat).style.backgroundColor = '';

    // Highlight newly selected seat
    document.getElementById('' + tableName + seatNum + '').style.backgroundColor = 'orange';

    // Update hidden field value with the table and seat IDs
    sb = tableID + ":" + seatID;
    document.getElementById("MainContent_Hidden1").value = sb;

    // Update the message to the user
    sb = "Please confirm your selection of seat " + seatNum + " at table " + tableName + ". This will clear your other seat selection.";
    document.getElementById("MainContent_chosenMessage").innerHTML = sb;

    // Store the seat selected in case of a seat selection change
    lastSeat = '' + tableName + seatNum + '';
} */