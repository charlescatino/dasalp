﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SiteSettings.aspx.cs" Inherits="Admin_SiteSettings" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" Runat="Server">
    <form id="SiteSettingsForm" runat="server">
        <div class="accountInfo">
            <fieldset class="settings">
                <legend>Site Settings</legend>
                <p>
                    <asp:Label ID="EventNameLabel" runat="server" AssociatedControlID="EventName">Event Name:</asp:Label><br />
                    <asp:TextBox ID="EventName" runat="server" CssClass="textEntry"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="PaypalEmailLabel" runat="server" AssociatedControlID="PaypalEmail">Paypal Email:</asp:Label><br />
                    <asp:TextBox ID="PaypalEmail" runat="server" CssClass="textEntry"></asp:TextBox>
                </p>
                <p>
                    <asp:Label ID="LayoutIDLabel" runat="server" AssociatedControlID="LayoutID">Set Layout:</asp:Label>
                    <asp:DropDownList ID="LayoutID" runat="server"></asp:DropDownList><br />
                    <asp:Label ID="ActiveLayoutIDLabel" runat="server"></asp:Label>
                </p>
                <h2>MORE</h2>
            </fieldset>
        </div>
        <asp:Button ID="saveChanges" runat="server" Text="Save Changes" OnClick="saveSiteChanges" />
    </form>
</asp:Content>