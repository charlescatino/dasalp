﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_SiteSettings : System.Web.UI.Page
{
    public layout currLayout = new layout();
    protected void Page_Load(object sender, EventArgs e)
    {
        loadSettingInfo();
        populateLayouts();
    }

    private void loadSettingInfo()
    {
        //EventName.Text = Global.SiteTitle;
        PaypalEmail.Text = Global.PaypalEmail;
    }

    public void saveSiteChanges(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        if (EventName.Text != "")
        {
            String newEventName = EventName.Text.ToString();
            Global.SiteTitle = newEventName;
        }
        if (PaypalEmail.Text != "")
        {
            String newPaypalEmail = PaypalEmail.Text.ToString();
            Global.PaypalEmail = newPaypalEmail;
        }
        if (LayoutID.SelectedIndex != 0)
        {
            currLayout = (from lo in db.layouts
                          where lo.id == Convert.ToInt32(LayoutID.SelectedValue)
                          select lo).Single();
            Global.LayoutID = currLayout.id;
        }
        // TODO: (hack) This shouldn't just redirect...
        Response.Redirect("SiteSettings.aspx");
    }
    protected void populateLayouts()
    {
        this.LayoutID.Items.Add(new ListItem("Set Seating Layout", "0"));
        DataClassesDataContext db = new DataClassesDataContext();
        var query = (from lo in db.layouts
                     select lo).ToList();
        foreach (layout lo in query)
        {
            this.LayoutID.Items.Add(new ListItem((lo.name.ToString()), lo.id.ToString()));
        }
        currLayout = (from lo in db.layouts
                      where lo.id == Convert.ToInt32(Global.LayoutID)
                      select lo).Single();
        ActiveLayoutIDLabel.Text = "Currently Active Layout: " + currLayout.name;
    }
}