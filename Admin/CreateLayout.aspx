﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CreateLayout.aspx.cs" Inherits="Admin_CreateLayout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <!-- Scripts -->
    <script language="javascript" type="text/javascript" src="./js/CreateLayout.js"></script>
    <script language="javascript" type="text/javascript" src="./js/Table.js"></script>
    <script language="javascript" type="text/javascript" src="../Scripts/jquery-1.js"></script>
    <script language="javascript" type="text/javascript" src="../Scripts/jquery-ui.js"></script>

    <script language="javascript" type="text/javascript">
        $(document).ready(function(){
            <%Response.Write(this.populateTables());%>
        });
    </script>

    <form id="form1" runat="server">
    <input ID="Hidden1" runat="server" type="hidden" />
    <asp:DropDownList ID="layoutDD" runat="server" AutoPostBack="True" OnSelectedIndexChanged="loadLayout">
    </asp:DropDownList>

    <div class="container" id="layoutArea" style="display: block; position:relative; ">
    </div>

    <div id="rightCreateLayout">
        <fieldset class="controls">
            <legend>Controls</legend>
            <table>
                <tr>
                    <td>Length:</td>
                    <td><asp:DropDownList ID="tableLength" runat="server"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>Width:</td>
                    <td><asp:DropDownList ID="tableWidth" runat="server">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem Selected="True">2</asp:ListItem>
                    </asp:DropDownList></td>
                </tr>
                <tr>
                    <td>Orientation:</td>
                    <td><asp:DropDownList ID="tableOrientation" runat="server">
                        <asp:ListItem>Vertical</asp:ListItem>
                        <asp:ListItem>Horizontal</asp:ListItem>
                    </asp:DropDownList></td>
                </tr>
                <tr>
                    <td><button type="button" onclick="addTable()">Add Table</button></td>
                    <td><button type="button" onclick="clearTables()">Clear Tables</button></td>
                </tr>
            </table>
        </fieldset>
        <fieldset class="settings">
            <legend>Settings</legend>
            <p>
                <asp:Label ID="TableStepLabel" runat="server" Text="Table Step:"></asp:Label><br />
                <asp:DropDownList ID="TableStepDDL" CssClass="textEntry" runat="server">
                    <asp:ListItem>Alpha (A-Z)</asp:ListItem>
                    <asp:ListItem>Numerical (1-#)</asp:ListItem>
                </asp:DropDownList>
            </p>
            <p>
                <asp:Label ID="SeatStepLabel" runat="server" Text="Seat Step:"></asp:Label><br />
                <asp:DropDownList ID="SeatStepDDL" CssClass="textEntry" runat="server">
                    <asp:ListItem>Numerical (1-#)</asp:ListItem>
                    <asp:ListItem>Alpha (A-Z)</asp:ListItem>
                </asp:DropDownList>
            </p>
            <p>
                <asp:Label ID="SeatCostLabel" runat="server" Text="Seat Cost:"></asp:Label>
                <asp:TextBox ID="SeatCost" CssClass="textEntryShort" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="SeatCostValidator" runat="server" ErrorMessage="You must enter a seat cost" ControlToValidate="SeatCost" ></asp:RequiredFieldValidator>
            </p>
            <p>
                <asp:Label ID="LayoutNameLabel" runat="server" Text="Layout Name:"></asp:Label>
                <asp:TextBox ID="LayoutName" CssClass="textEntryShort" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="LayoutNameValidator" runat="server" ErrorMessage="You must enter a layout name" ControlToValidate="LayoutName" ></asp:RequiredFieldValidator>
            </p>
            <p>
                <asp:Button ID="saveLayout" runat="server" onclick="saveClick" OnClientClick="if (!saveLayout()) return false;" Text="Save Layout" Disabled="true" />
            </p>
         </fieldset>
    </div>
    </form>
</asp:Content>