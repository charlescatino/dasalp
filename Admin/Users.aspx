﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Users.aspx.cs" Inherits="Admin_Users" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" Runat="Server">
<form runat="server">
    <asp:LinqDataSource ID="UserDS" runat="server" 
        ContextTypeName="DataClassesDataContext" EnableDelete="True" 
        EnableInsert="True" EnableUpdate="True" EntityTypeName="" 
        TableName="users" OrderBy="lastName">
    </asp:LinqDataSource>
    <asp:GridView ID="Users" runat="server" AllowSorting="True" 
        AutoGenerateColumns="False" DataKeyNames="id" 
        DataSourceID="UserDS" 
        onrowcommand="gvUsers_OnRowCommand" AllowPaging="True" PageSize="30">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="lastName" HeaderText="Last" 
                SortExpression="lastName" />
            <asp:BoundField DataField="firstName" HeaderText="First" 
                SortExpression="firstName" />
            <asp:BoundField DataField="username" HeaderText="Username" 
                SortExpression="username" />
            <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />
            <asp:CheckBoxField DataField="isAdmin" HeaderText="Admin?" 
                SortExpression="isAdmin" />
            <asp:CheckBoxField DataField="member" HeaderText="Member?" 
                SortExpression="member" />
        </Columns>
    </asp:GridView>
    <br />
    <br />
    <br />
</form>
</asp:Content>