﻿/* 
CreateLayout

Used to handle all the Javascript stuff for creating and arranging table layouts

*/

var numTables = 0;
var yGrid = 20;
var xGrid = 20;
tables = new Array();
sortedTable = new Array();

window.onload = function () {
    // reserved
}

function addTable() {
    var or = document.getElementById("MainContent_tableOrientation");
    var strOrientation = or.options[or.selectedIndex].text;
    var len = document.getElementById("MainContent_tableLength");
    var tableLength = len.options[len.selectedIndex].text;
    var width = document.getElementById("MainContent_tableWidth");
    var tableWidth = width.options[width.selectedIndex].text;
    placeTable(tableLength, tableWidth, strOrientation, 0, 0);
    document.getElementById("MainContent_saveLayout").disabled = false;
}

function placeTable(length, width, orientation, xStart, yStart) {
    row = new Array();
    cell = new Array();
    row_num = 0;
    cell_num = 0;
    if (orientation == "Horizontal") {
        row_num = width;
        cell_num = length;
    }
    else {
        row_num = length;
        cell_num = width;
    }
    tab = document.createElement('table');
    tab.setAttribute('id', 'draggable' + numTables);
    tab.setAttribute('class', 'tableDrag ui-draggable');
    tab.setAttribute('onmousedown', 'setPosition(this)');
    tab.setAttribute('style', 'left: ' + xStart + ' px; top: ' + yStart + ' px; position: absolute;');
    //tab.setAttribute('id', 'newtable');
    tbo = document.createElement('tbody');
    for (i = 0; i < row_num; i++) {
        row[i] = document.createElement('tr');
        for (j = 0; j < cell_num; j++) {
            cell[j] = document.createElement('td');
            cell[j].setAttribute('class', 'tdDrag');
            //cont = document.createTextNode((i + 1) * (j + 1));
            //cell[j].appendChild(cont);
            row[i].appendChild(cell[j]);
        }
        tbo.appendChild(row[i]);
    }
    tab.appendChild(tbo);

    document.getElementById('layoutArea').appendChild(tab);

    tables.push(new Table(orientation, length, width, 'draggable' + numTables));
    tables[tables.length - 1].setCoords(0, 0);

    numTables++;
}


// Written by Quintin Cummins
function setPosition(control) {
    control.style.backgroundColor = "black";
    $("#" + control.id).draggable({ zIndex: 2700 });
    $("#" + control.id).draggable({ grid: [xGrid, yGrid] });
    $("#" + control.id).draggable({
        stop: function (event, ui) {
            Stoppos = $(this).position();
            $("#box2").val(Stoppos.left + "," + Stoppos.top);
            control.style.backgroundColor = "white";
            // update table in array with the proper coordinates
            tables[indexOfTable(control.id)].setCoords(Stoppos.left, Stoppos.top);
        }
    });
}

/* Iterate through tables and return the index of the table 
 * for the given tableID (the HTML ID tag of the table
 */
function indexOfTable(tableID) {
    for (var i=0; i < tables.length; i++) {
        if (tables[i].id == tableID)
        return i;
    }
}

/* This function clears all of the tables from the Layout Area div */
function clearTables() {
    var i;
    document.getElementById('layoutArea').innerHTML = "";
    tables.length = 0;
    document.getElementById("MainContent_saveLayout").disabled = true;
}

/* Iterate through the tables and create a message to pass back to 
 * the C# backend to store table data
 */
function saveLayout() {
    // check to make sure there are tables
    if (tables.length == 0)
        return false;

    // sort the tables by position (xCoord), then (yCoord)
    tables.sort(sort_by('xCoord', 'yCoord'));

    // build string to pass to C# via hidden field
    // FORMAT: "orientation:length:width:leftOffset:topOffset;REPEAT"
    var sb = "";
    for (var t in tables) {
        sb = sb + tables[t].orientation + ":" + tables[t].length + ":" + tables[t].width + ":";
        sb += document.getElementById(tables[t].id).offsetLeft + ":";
        sb += document.getElementById(tables[t].id).offsetTop + ";";
    }
    //alert(sb);
    document.getElementById("MainContent_Hidden1").value = sb;
    //alert(document.getElementById("MainContent_Hidden1").value);
    return true;
}

/* Sorting function stuff used to sort the tables
 * Stolen from here:
 * http://stackoverflow.com/questions/6913512/how-to-sort-an-array-of-objects-by-multiple-fields
 */

var sort_by;

(function () {
    // utility functions
    var default_cmp = function (a, b) {
        if (a == b) return 0;
        return a < b ? -1 : 1;
    },
        getCmpFunc = function (primer, reverse) {
            var dfc = default_cmp, // closer in scope
                cmp = default_cmp;
            if (primer) {
                cmp = function (a, b) {
                    return dfc(primer(a), primer(b));
                };
            }
            if (reverse) {
                return function (a, b) {
                    return -1 * cmp(a, b);
                };
            }
            return cmp;
        };

    // actual implementation
    sort_by = function () {
        var fields = [],
            n_fields = arguments.length,
            field, name, reverse, cmp;

        // preprocess sorting options
        for (var i = 0; i < n_fields; i++) {
            field = arguments[i];
            if (typeof field === 'string') {
                name = field;
                cmp = default_cmp;
            }
            else {
                name = field.name;
                cmp = getCmpFunc(field.primer, field.reverse);
            }
            fields.push({
                name: name,
                cmp: cmp
            });
        }

        // final comparison function
        return function (A, B) {
            var a, b, name, result;
            for (var i = 0; i < n_fields; i++) {
                result = 0;
                field = fields[i];
                name = field.name;

                result = field.cmp(A[name], B[name]);
                if (result !== 0) break;
            }
            return result;
        }
    }
} ());


/* Debug stuff below here... */
function showPosition(control) {
    var left;
    var top;
    var i;
    for (i = 0; i < tables.length; i++) {
        var left = document.getElementById('draggable' + i).offsetLeft - startX;
        var top = document.getElementById('draggable' + i).offsetTop - startY;
        alert(left + ", " + top);
    }
}

function addText() {
    var txt = 'my new text';
    var newText = document.createTextNode(txt);
    //alert(txt);
    document.getElementByID('MainContent_myPanel').appendChild(txt);
}