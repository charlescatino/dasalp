﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Layouts.aspx.cs" Inherits="Admin_js_Layouts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <form id="form1" runat="server">
    <asp:LinqDataSource ID="LinqDataSource1" runat="server" 
        ContextTypeName="DataClassesDataContext" EnableDelete="True" 
        EnableUpdate="True" EntityTypeName="" TableName="layouts">
    </asp:LinqDataSource>
    <asp:GridView ID="Layouts" runat="server" AllowSorting="True" AutoGenerateColumns="False" 
        DataKeyNames="id" DataSourceID="LinqDataSource1" 
        onrowcommand="gvUsers_OnRowCommand">
        <Columns>
            <asp:BoundField DataField="name" HeaderText="Layout Name" 
                SortExpression="name" />
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
        </Columns>
    </asp:GridView>
    </form>
</asp:Content>

