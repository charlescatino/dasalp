﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class Admin_CreateLayout : System.Web.UI.Page
{
    public layout currLayout = new layout();

    protected void Page_Load(object sender, EventArgs e)
    {
        populateLength();
        populateLayouts();
        if (Session["Layout"] != null && Session["Layout"] != currLayout)
        {
            currLayout = (layout)Session["Layout"];
        }
    }

    protected void populateLength()
    {
        int i;
        for (i = 3; i <= 20; i++)
            this.tableLength.Items.Add(new ListItem((i).ToString()));
    }
    
    protected void populateLayouts()
    {
        this.layoutDD.Items.Add(new ListItem("Load A Layout (not working)", "0"));
        DataClassesDataContext db = new DataClassesDataContext();
        var query = (from lo in db.layouts
                     select lo).ToList();
        foreach (layout lo in query)
        {
            this.layoutDD.Items.Add(new ListItem((lo.name.ToString()), lo.id.ToString()));
        }
    }

    protected void loadLayout(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        var query = (from lo in db.layouts
                     where lo.id == Convert.ToInt32(layoutDD.SelectedValue)
                     select lo).Single();
        Session["Layout"] = (layout)query;
        Response.Redirect("CreateLayout.aspx");
    }

    protected string populateTables()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("");
        foreach (table tbl in currLayout.tables)
        {
            sb.Append("placeTable(" + tbl.length + ", "
                + tbl.width + ", "
                + "\"" + tbl.orientation + "\"" + ", "
                + tbl.leftOffset + ", "
                + tbl.topOffset + ");");
        }
        return sb.ToString();
    }

    protected void saveClick(object sender, EventArgs e)
    {
        save();
    }

    protected void save()
    {
        int i, numSeats = 0; // table loop, counter for number of seats
        string tableStep, seatStep;
        // Determine table step type
        if (TableStepDDL.SelectedIndex == 0)
            tableStep = "A"; // A -> Z  , TODO: Account for past Z?
        else if (TableStepDDL.SelectedIndex == 1)
            tableStep = "Z"; // Starting at "Z" unless there are more than 26 tables?
        else
            tableStep = "1"; // Numerical
        // Determine seat step type
        seatStep = setSeatStep();

        DataClassesDataContext db = new DataClassesDataContext();

        // If existing layout, delete everything pertaining to it before adding more
        if (currLayout.name != null && this.LayoutName.Text == null)
        {
            var delTables = (from tbl in db.tables
                             where tbl.layoutID == currLayout.id
                             select tbl).ToList();

            db.tables.DeleteAllOnSubmit(delTables);

            foreach (table tbl in delTables)
            {
                var delSeats = (from st in db.seats
                                where st.tableID == tbl.id
                                select st).ToList();
                db.seats.DeleteAllOnSubmit(delSeats);
            }
            db.SubmitChanges();
        }
        else // create new layout with name
        {
            layout lo = new layout
            {
                name = this.LayoutName.Text
            };
            db.layouts.InsertOnSubmit(lo);
            db.SubmitChanges();
            // Query for newly added Layout
            currLayout = (layout)(from lot in db.layouts
                                  where lot.id == lo.id
                                  select lot).Single();
        }

        // Get string from Javascript seat builder
        // FORMAT: "orientation:length:width:leftOffset:topOffset;REPEAT"
        string[] tables = Hidden1.Value.TrimEnd(';').Split(';');
        foreach (string table in tables)
        {
            string[] table_info = table.Split(':');
            table tbl = new table
            {
                layoutID = currLayout.id,
                name = tableStep,
                orientation = table_info[0],
                length = Convert.ToInt32(table_info[1]),
                width = Convert.ToInt32(table_info[2]),
                leftOffset = Convert.ToInt32(table_info[3]),
                topOffset = Convert.ToInt32(table_info[4])
            };
            db.tables.InsertOnSubmit(tbl);
            tableStep = incrementStep(tableStep);
        }
        db.SubmitChanges();

        // Pull tables back out to make seats for each one
        // Could avoid this by grabbing each table one by one before,
        // or possibly putting table IDs into an array?
        var query = (from tbl in db.tables
                     where tbl.layoutID == currLayout.id
                     select tbl).ToList();

        foreach (table tbl in query)
        {
            for (i = 1; i <= tbl.length * tbl.width; i++)
            {
                seat st = new seat
                {
                    name = seatStep,
                    tableID = tbl.id,
                    cost = Convert.ToInt32(SeatCost.Text)
                };
                db.seats.InsertOnSubmit(st);
                seatStep = incrementStep(seatStep);
                numSeats++;
            }
            seatStep = setSeatStep();
        }

        // Update the layout with the number of seats
        var layoutQuery = (from lyt in db.layouts
                     where lyt.id == currLayout.id
                     select lyt).Single();
        layoutQuery.numSeats = numSeats;
        db.SubmitChanges();
        Session["Layout"] = currLayout;
        Response.Redirect("~/Admin/CreateLayout.aspx");
    }

    protected string incrementStep(string stepValue)
    {
        // Attempt to convert stepValue to integer format
        int myNum;
        if (int.TryParse(stepValue, out myNum))
        {
            myNum++;
            return Convert.ToString(myNum);
        }
        // Attempt to convert to character format
        char myChar;
        if (char.TryParse(stepValue, out myChar))
        {
            myChar++;
            return Convert.ToString(myChar);
        }
        return "";
    }

    protected string setSeatStep()
    {
        if (SeatStepDDL.SelectedIndex == 0)
            return "1";  // Numerical
        else if (SeatStepDDL.SelectedIndex == 1)
            return "A"; // A->Z   , TODO: Account for past Z?
        else
            return "Z"; // Starting at "Z" unless there are more than 26 tables?
    }
}