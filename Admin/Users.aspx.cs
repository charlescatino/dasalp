﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Users : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /* Used to populate the org_users table upon creation...
        DataClassesDataContext db = new DataClassesDataContext();
        var currUsers = (from usr in db.users
                     select usr);
        foreach (user currUser in currUsers)
        {
            org_user newOrgUser = new org_user
            {
                org_id = 1,
                admin = false,
                user_id = currUser.id,
                member = false,
                staff = false
            };
            db.org_users.InsertOnSubmit(newOrgUser);
        }
        db.SubmitChanges(); */
    }

    protected void gvUsers_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        if (e.CommandName == "Delete")
        {
            // Delete all associated seat selections by this user
            var userSeats = (from st_usr in db.seat_users
                             where st_usr.user_id == Convert.ToInt32(e.CommandArgument.ToString())
                             select st_usr).ToList();
            db.seat_users.DeleteAllOnSubmit(userSeats);
            // Set all associated payments to 0 for this user
            var userPayments = (from pmt in db.payments
                                where pmt.userID == Convert.ToInt32(e.CommandArgument.ToString())
                                select pmt).ToList();
            foreach (payment pmt in userPayments)
            {
                pmt.userID = null;
            }
            db.SubmitChanges();
        }
    }
}