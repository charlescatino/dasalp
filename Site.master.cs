﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SiteMaster : System.Web.UI.MasterPage
{
    public user currUser = new user();
	public organization currOrg = new organization();
	public party currParty = new party();
    public bool admin = false;

	/*protected void Page_Init(object sender, EventArgs e) {
		// Path options:
		// ~/ORG/Page
		// ~/ORG/Admin/Page
		// ~/Account/Page
		// ~/ORG/PARTY/Page
		// ~/
		// REDO THIS... check for party and org at same time?
		// TODO: need to add a 'group' page...

		string path_url = Request.Url.AbsolutePath;
		string[] path_elements = new string[10];
		if (path_url != null)
			path_elements = path_url.ToLower().Substring(1).TrimEnd('/').Split('/');
		if (Session["org"] != null)
		{
			currOrg = (organization)Session["org"];
			if (currOrg.url_name != path_elements[1])
			{
				loadNewOrg(path_elements[1]);
			}
		}
		else
		{
			loadNewOrg(path_elements[1]);
		}

		if (Session["party"] != null)
		{
			currParty = (party)Session["party"];
			if (currParty.party_url != path_elements[2])
			{
				loadNewParty(path_elements[2]);
			}
		}
		else
		{
			loadNewParty(path_elements[2]);
		}
		Global._eventID = currParty.id;
	} */

	private void loadNewOrg(string org_url)
	{
		try
		{
			currOrg = Global.DAL.getOrgByURL(org_url);
			Session["org"] = currOrg;
		}
		catch
		{
			// invalid new org URL
		}
	}

	private void loadNewParty(string party_url)
	{
		try
		{
			currParty = Global.DAL.getPartyByURL(party_url);
			Session["party"] = currParty;
		}
		catch
		{
			// invalid new party URL
		}
	}

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check to see if anyone is logged in
        if (Session["user"] != null)
        {
            currUser = (user)Session["user"];
            admin = (bool)currUser.isAdmin;
            AdminLinks();
        }
        else
            Debug();

        // If admin page, check for admin user.
        if (HttpContext.Current.Request.Url.AbsoluteUri.Contains("Admin") && !admin)
        {
			Response.Redirect("~/Default.aspx");
        }

        LoginLink();
    }

    protected void LoginLink()
    {
        if (Session["user"] != null)
        {
            Label loginName = new Label();
            loginName.Text = "Logged in as: " + currUser.firstName + " "
                + currUser.lastName + " "
                + "(" + currUser.username + ")";
            PlaceHolderLoginStatus.Controls.Add(loginName);

            HyperLink hrf = new HyperLink();
            hrf.Text = "(Logout)";
            hrf.NavigateUrl = "~/Account/Logout.aspx";
            PlaceHolderLogout.Controls.Add(hrf);

            Label space = new Label();
            space.Text = "  ";
            PlaceHolderLogout.Controls.Add(space);

            HyperLink hrf2 = new HyperLink();
            hrf2.Text = "(Account Settings)";
            hrf2.NavigateUrl = "~/Account/Settings.aspx";
            PlaceHolderLogout.Controls.Add(hrf2);
        }
        else
        {
            HyperLink hrf = new HyperLink();
            hrf.Text = "Login";
            hrf.NavigateUrl = "~/Account/Login.aspx";
            PlaceHolderLoginStatus.Controls.Add(hrf);

            HyperLink hrfReg = new HyperLink();
            hrfReg.Text = "Create Account";
            hrfReg.NavigateUrl = "~/Account/CreateAccount.aspx";
            PlaceHolderLogout.Controls.Add(hrfReg);
        }
    }

    protected void AdminLinks()
    {
        if ((bool)currUser.isAdmin)
            AdminLinksPlaceholder.Visible = true;
    }

    public string getClientScript()
    {
        if ((bool)currUser.isAdmin)
            return "setAdminMenu();";
        return "";
    }

    protected void Debug()
    {
        // reserved for Debug user
    }
}
