﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Information : System.Web.UI.Page
{
    public bool admin = false;
    public user currUser = new user();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] != null)
        {
            currUser = (user)Session["user"];
            admin = (bool)currUser.isAdmin;
        }
        if (admin)
        {
            AddInformationPlaceHolder.Visible = true;
        }

        // Set session eventID to be used for the LinqDataSource connection
        Session["eventID"] = Convert.ToInt32(Global._eventID);
    }

    protected void addInformation(object sender, EventArgs e)
    {
        DataClassesDataContext db = new DataClassesDataContext();
        String informationText = InformationBody.Text;
        information newInformation = new information
        {
            body = InformationBody.Text,
            party_id = Convert.ToInt32(Global._eventID),
            title = InformationTitle.Text,
            weight = Convert.ToInt32(InformationWeight.Text)
        };
        db.informations.InsertOnSubmit(newInformation);
        db.SubmitChanges();
    }
}