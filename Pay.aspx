﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Pay.aspx.cs" Inherits="Pay" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" Runat="Server">
    <!-- Scripts -->
    <script language="javascript" type="text/javascript">
        /*function populateCustomValue(userID) {
            // Set Paypal custom hidden value to "<userID>:<selected dropdown index>"
            document.getElementById("HiddenPaypal").value = (userID);
        } */
    </script>
    <div class="checkoutInfo">
        <fieldset class="checkout">
            <legend>Checkout</legend>
            <p>
                You currently have <% Response.Write(userSeats.Count()); %> seats reserved, totaling to $<% Response.Write(seatTotal); %>.
            </p>
            <% if (getsDiscount)
               { %>
               <p>
                    You are a member! That's awesome! Take $5 off of your total.<br />
                    Final total: $<%= (seatTotal)%>
               </p>
            <% } %>
            <asp:PlaceHolder ID="LivePaypal" runat="server" Visible="false">
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="business" value="catinoc@onid.orst.edu">
                <input type="hidden" name="item_name" value="<% Response.Write(Global.SiteTitle); %> Payment">
                <input type="hidden" name="amount" value="<% Response.Write(seatTotal); %>" />
                <input type="hidden" name="custom" id="HiddenPaypal" value="<% Response.Write(currUser.id); %>" />
                <input type="hidden" name="currency_code" value="USD">
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="TestPaypal" runat="server" Visible="false">
                <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="business" value="W4PDGUQUNXJ72">
                <input type="hidden" name="lc" value="US">
                <input type="hidden" name="item_name" value="<% Response.Write(Global.SiteTitle); %> Payment">
                <input type="hidden" name="amount" value="<% Response.Write(seatTotal); %>" />
                <input type="hidden" name="custom" id="HiddenPaypal" value="<% Response.Write(currUser.id); %>" />
                <input type="hidden" name="currency_code" value="USD">
                <input type="hidden" name="button_subtype" value="services">
                <input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
                <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
            </asp:PlaceHolder>
        </fieldset>
    </div>
</asp:Content>