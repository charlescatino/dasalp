﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Tournaments.aspx.cs" Inherits="Tournaments" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Tournaments</h2>
    <form id="form1" runat="server">

    <asp:LinqDataSource ID="TournamentDS" runat="server" 
        ContextTypeName="DataClassesDataContext" EnableDelete="True" 
        EnableUpdate="True" EntityTypeName="" TableName="tournaments" 
        Where="party_id == @party_id">
        <WhereParameters>
            <asp:SessionParameter Name="party_id" SessionField="eventID" Type="Int32" />
        </WhereParameters>
    </asp:LinqDataSource>
    <asp:ListView ID="TournamentLV" runat="server" DataKeyNames="id" 
        DataSourceID="TournamentDS">
        <EmptyDataTemplate>
            <span>There are currently no tournaments for this event...</span>
        </EmptyDataTemplate>
        <ItemTemplate>
            <h2><asp:Label ID="nameLabel" runat="server"><%# Eval("name") %><%= " " %><%# Eval("teamSize") %>v<%# Eval("teamSize") %></asp:Label></h2>
            <br />
            Rules: <asp:Label ID="rulesLabel" runat="server" Text='<%# Eval("rules") %>' /><br />
            Signup: <asp:Label ID="signupLabel" runat="server" Text='<%# Eval("signup") %>' /><br />
            Start Time: <asp:Label ID="startTimeLabel" runat="server" Text='<%# Eval("startTime") %>' />
        </ItemTemplate>
    </asp:ListView>

    <!--
<% foreach (tournament trny in this.tourneys)
   { %>
        <h2><% Response.Write(trny.name); %></h2>
        <% if (trny.rules != null)
           { %>
               <p>
                <a href="<% Response.Write(trny.rules); %>">Tournament Rules</a>
        <% }
           else
           { %>
               <p>Rules coming soon.
               <% }
           if (trny.signup != null)
           { %>
                | <a href="<% Response.Write(trny.signup); %>">Tournament Signup</a>
        <% }
       else
       { %>
            | Signups coming soon.
            <% }
            if (trny.rules != null)
            { %>
            </p>
        <% }
       
   } %> -->
    </form>
</asp:Content>